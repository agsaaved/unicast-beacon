# Author: Andres Garcia Saavedra #

This repository includes the patches to support unicast beacons in mac80211. The code is based on backports 3.13.2-1.

* [net/mac80211/debugfs_sta.c] Creates a directory per associated STA with individual information. This patch implements writing/reading for individual WMM parameters, and reading accumulated rx airtime and packets. To write individual WMM parameters: echo <queue> <cwmin/cwmax/aifsn/txopp> <value> > ags_unicast_wme. To activate/deactivate unicast beacons: echo 0/1 > ags_unicast_wme.
* [net/mac80211/tx.c] When the template of a beacon is grabbed (ieee80211_beacon_get_tim()), a copy of the template is created and passed to a new function (ags_tx_individual_beacon()), in charge of modifying the destination address and individual WMM parameters accordingly, and send it to the CAB queue. The DTIM flag is set to activate delivery of the CAB queue after a beacon is sent.
* [net/mac80211/rx.c] Updates the accumulated rx airtime per station (implementation is done in net/mac80211/util.c).
* [net/mac80211/sta_info.c/h] Variables and structs to support individual WMM information.